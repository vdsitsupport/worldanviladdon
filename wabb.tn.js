// ==UserScript==
// @name         WA BB
// @namespace    http://tampermonkey.net/
// @version      0.0.3
// @description  try to take over the world!
// @author       You
// @match        https://www.worldanvil.com/*
// @grant        GM_addStyle
// @resource     jqueryui https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css
// @require      https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js
// @require      https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js
// @require      https://cdn.jsdelivr.net/npm/codemirror@5.50.0/lib/codemirror.min.js
// @require      https://cdn.jsdelivr.net/npm/codemirror@5.50.0/addon/mode/simple.js
// @require      https://cdn.jsdelivr.net/npm/codemirror@5.50.0/addon/display/panel.js
// @require      https://cdn.jsdelivr.net/npm/codemirror@5.50.0/addon/hint/show-hint.js
// @require      https://cdn.jsdelivr.net/npm/codemirror@5.50.0/addon/edit/matchtags.js
// @require      https://cdn.jsdelivr.net/npm/codemirror@5.50.0/addon/hint/javascript-hint.js

// ==/UserScript==
var codemirrorInstance; // array of all instances.
var sidebarcontent = "<div>CSS Classes: <br>col-md-* <br>\
col-md-push-*  <br>\
col-md-pull-*";
var unsaved = false;
// on the clicking of nav-links, refresh all codemirror instances. Else the sides stay wonky wonky.
$( document ).ready(function() {
    $(".nav-link").click(function() {
        $.each(codemirrorInstance, function(i,e) { e.refresh() });
    });
});

// Create editor mode
CodeMirror.defineMode("bbcode", function(config) {
    var settings, m, last;

    settings = {
        bbCodeTags: "b i u s img quote code list table  tr td size color url",
        bbCodeUnaryTags: "* :-) hr cut"
    };

    if (config.hasOwnProperty("bbCodeTags")) {
        settings.bbCodeTags = config.bbCodeTags;
    }
    if (config.hasOwnProperty("bbCodeUnaryTags")) {
        settings.bbCodeUnaryTags = config.bbCodeUnaryTags;
    }

    var helpers = {
        cont: function(style, lastType) {
            last = lastType;
            return style;
        },
        escapeRegEx: function(s) {
            return s.replace(/([\:\-\)\(\*\+\?\[\]])/g, '\\$1');
        }
    };

    var regs = {
        validIdentifier: /[a-zA-Z0-9_]/,
        stringChar: /['"]/,
        tags: new RegExp("(?:" + helpers.escapeRegEx(settings.bbCodeTags).split(" ").join("|") + ")"),
        unaryTags: new RegExp("(?:" + helpers.escapeRegEx(settings.bbCodeUnaryTags).split(" ").join("|") + ")")
    };

    var parsers = {
        tokenizer: function (stream, state) {
            if (stream.eatSpace()) return null;

            if (stream.match('[', true)) {
                state.tokenize = parsers.bbcode;
                return helpers.cont("tag", "startTag");
            }

            stream.next();
            return null;
        },
        inAttribute: function(quote) {
            return function(stream, state) {
                var prevChar = null;
                var currChar = null;
                while (!stream.eol()) {
                    currChar = stream.peek();
                    if (stream.next() == quote && prevChar !== '\\') {
                        state.tokenize = parsers.bbcode;
                        break;
                    }
                    prevChar = currChar;
                }
                return "string";
            };
        },
        bbcode: function (stream, state) {
            if (m = stream.match(']', true)) {
                state.tokenize = parsers.tokenizer;
                return helpers.cont("tag", null);
            }

            if (stream.match('[', true)) {
                return helpers.cont("tag", "startTag");
            }

            var ch = stream.next();
            if (regs.stringChar.test(ch)) {
                state.tokenize = parsers.inAttribute(ch);
                return helpers.cont("string", "string");
            } else if (/\d/.test(ch)) {
                stream.eatWhile(/\d/);
                return helpers.cont("number", "number");
            } else {
                if (state.last == "whitespace") {
                    stream.eatWhile(regs.validIdentifier);
                    return helpers.cont("attribute", "modifier");
                } if (state.last == "property") {
                    stream.eatWhile(regs.validIdentifier);
                    return helpers.cont("property", null);
                } else if (/\s/.test(ch)) {
                    last = "whitespace";
                    return null;
                }

                var str = "";
                if (ch != "/") {
                    str += ch;
                }
                var c = null;
                while (c = stream.eat(regs.validIdentifier)) {
                    str += c;
                }
                if (regs.unaryTags.test(str)) {
                    return helpers.cont("atom", "atom");
                }
                if (regs.tags.test(str)) {
                    return helpers.cont("keyword", "keyword");
                }
                if (/\s/.test(ch)) {
                    return null;
                }
                return helpers.cont("tag", "tag");
            }
        }
    };

    return {
        startState: function() {
            return {
                tokenize: parsers.tokenizer,
                mode: "bbcode",
                last: null
            };
        },
        token: function(stream, state) {
            var style = state.tokenize(stream, state);
            state.last = last;
            return style;
        },
        electricChars: ""
    };
});

CodeMirror.defineMIME("text/x-bbcode", "bbcode");

// Sneaking in modified xml-fold.js

(function(mod) {
  if (typeof exports == "object" && typeof module == "object") // CommonJS
    mod(require("../../lib/codemirror"));
  else if (typeof define == "function" && define.amd) // AMD
    define(["../../lib/codemirror"], mod);
  else // Plain browser env
    mod(CodeMirror);
})(function(CodeMirror) {
  "use strict";

  var Pos = CodeMirror.Pos;
  function cmp(a, b) { return a.line - b.line || a.ch - b.ch; }

  var nameStartChar = "\\w";
  var nameChar = nameStartChar + "\-\:\.0-9\\u00B7\\u0300-\\u036F\\u203F-\\u2040";
  var xmlTagStart = new RegExp("\\[(/?)([" + nameStartChar + "][" + nameChar + "]*)", "g");

  function Iter(cm, line, ch, range) {
    this.line = line; this.ch = ch;
    this.cm = cm; this.text = cm.getLine(line);
    this.min = range ? Math.max(range.from, cm.firstLine()) : cm.firstLine();
    this.max = range ? Math.min(range.to - 1, cm.lastLine()) : cm.lastLine();
  }

  function tagAt(iter, ch) {
    var type = iter.cm.getTokenTypeAt(Pos(iter.line, ch));
    return type && /\btag\b/.test(type);
  }

  function nextLine(iter) {
    if (iter.line >= iter.max) return;
    iter.ch = 0;
    iter.text = iter.cm.getLine(++iter.line);
    return true;
  }
  function prevLine(iter) {
    if (iter.line <= iter.min) return;
    iter.text = iter.cm.getLine(--iter.line);
    iter.ch = iter.text.length;
    return true;
  }

  function toTagEnd(iter) {
    for (;;) {
      var gt = iter.text.indexOf("]", iter.ch);
      if (gt == -1) { if (nextLine(iter)) continue; else return; }
      if (!tagAt(iter, gt + 1)) { iter.ch = gt + 1; continue; }
      var lastSlash = iter.text.lastIndexOf("/", gt);
      var selfClose = lastSlash > -1 && !/\S/.test(iter.text.slice(lastSlash + 1, gt));
      iter.ch = gt + 1;
      return selfClose ? "selfClose" : "regular";
    }
  }
  function toTagStart(iter) {
    for (;;) {
      var lt = iter.ch ? iter.text.lastIndexOf("[", iter.ch - 1) : -1;
      if (lt == -1) { if (prevLine(iter)) continue; else return; }
      if (!tagAt(iter, lt + 1)) { iter.ch = lt; continue; }
      xmlTagStart.lastIndex = lt;
      iter.ch = lt;
      var match = xmlTagStart.exec(iter.text);
      if (match && match.index == lt) return match;
    }
  }

  function toNextTag(iter) {
    for (;;) {
      xmlTagStart.lastIndex = iter.ch;
      var found = xmlTagStart.exec(iter.text);
      if (!found) { if (nextLine(iter)) continue; else return; }
      if (!tagAt(iter, found.index + 1)) { iter.ch = found.index + 1; continue; }
      iter.ch = found.index + found[0].length;
      return found;
    }
  }
  function toPrevTag(iter) {
    for (;;) {
      var gt = iter.ch ? iter.text.lastIndexOf("]", iter.ch - 1) : -1;
      if (gt == -1) { if (prevLine(iter)) continue; else return; }
      if (!tagAt(iter, gt + 1)) { iter.ch = gt; continue; }
      var lastSlash = iter.text.lastIndexOf("/", gt);
      var selfClose = lastSlash > -1 && !/\S/.test(iter.text.slice(lastSlash + 1, gt));
      iter.ch = gt + 1;
      return selfClose ? "selfClose" : "regular";
    }
  }

  function findMatchingClose(iter, tag) {
    var stack = [];
    for (;;) {
      var next = toNextTag(iter), end, startLine = iter.line, startCh = iter.ch - (next ? next[0].length : 0);
      if (!next || !(end = toTagEnd(iter))) return;
      if (end == "selfClose") continue;
      if (next[1]) { // closing tag
        for (var i = stack.length - 1; i >= 0; --i) if (stack[i] == next[2]) {
          stack.length = i;
          break;
        }
        if (i < 0 && (!tag || tag == next[2])) return {
          tag: next[2],
          from: Pos(startLine, startCh),
          to: Pos(iter.line, iter.ch)
        };
      } else { // opening tag
        stack.push(next[2]);
      }
    }
  }
  function findMatchingOpen(iter, tag) {
    var stack = [];
    for (;;) {
      var prev = toPrevTag(iter);
      if (!prev) return;
      if (prev == "selfClose") { toTagStart(iter); continue; }
      var endLine = iter.line, endCh = iter.ch;
      var start = toTagStart(iter);
      if (!start) return;
      if (start[1]) { // closing tag
        stack.push(start[2]);
      } else { // opening tag
        for (var i = stack.length - 1; i >= 0; --i) if (stack[i] == start[2]) {
          stack.length = i;
          break;
        }
        if (i < 0 && (!tag || tag == start[2])) return {
          tag: start[2],
          from: Pos(iter.line, iter.ch),
          to: Pos(endLine, endCh)
        };
      }
    }
  }

  CodeMirror.registerHelper("fold", "xml", function(cm, start) {
    var iter = new Iter(cm, start.line, 0);
    for (;;) {
      var openTag = toNextTag(iter)
      if (!openTag || iter.line != start.line) return
      var end = toTagEnd(iter)
      if (!end) return
      if (!openTag[1] && end != "selfClose") {
        var startPos = Pos(iter.line, iter.ch);
        var endPos = findMatchingClose(iter, openTag[2]);
        return endPos && cmp(endPos.from, startPos) > 0 ? {from: startPos, to: endPos.from} : null
      }
    }
  });
  CodeMirror.findMatchingTag = function(cm, pos, range) {
    var iter = new Iter(cm, pos.line, pos.ch, range);
    if (iter.text.indexOf("]") == -1 && iter.text.indexOf("[") == -1) return;
    var end = toTagEnd(iter), to = end && Pos(iter.line, iter.ch);
    var start = end && toTagStart(iter);
    if (!end || !start || cmp(iter, pos) > 0) return;
    var here = {from: Pos(iter.line, iter.ch), to: to, tag: start[2]};
    if (end == "selfClose") return {open: here, close: null, at: "open"};

    if (start[1]) { // closing tag
      return {open: findMatchingOpen(iter, start[2]), close: here, at: "close"};
    } else { // opening tag
      iter = new Iter(cm, to.line, to.ch, range);
      return {open: here, close: findMatchingClose(iter, start[2]), at: "open"};
    }
  };

  CodeMirror.findEnclosingTag = function(cm, pos, range, tag) {
    var iter = new Iter(cm, pos.line, pos.ch, range);
    for (;;) {
      var open = findMatchingOpen(iter, tag);
      if (!open) break;
      var forward = new Iter(cm, pos.line, pos.ch, range);
      var close = findMatchingClose(forward, open.tag);
      if (close) return {open: open, close: close};
    }
  };

  // Used by addon/edit/closetag.js
  CodeMirror.scanForClosingTag = function(cm, pos, name, end) {
    var iter = new Iter(cm, pos.line, pos.ch, end ? {from: 0, to: end} : null);
    return findMatchingClose(iter, name);
  };
});

// alternatives here
var comp = [
    ["here", "hither"],
    ["asynchronous", "nonsynchronous"],
    ["completion", "achievement", "conclusion", "culmination", "expirations"],
    ["hinting", "advive", "broach", "imply"],
    ["function","action"],
    ["provide", "add", "bring", "give"],
    ["synonyms", "equivalents"],
    ["words", "token"],
    ["each", "every"],
]

// Autocomplete function here
function synonyms(cm, callback, option) {
    var cursor = cm.getCursor(), line = cm.getLine(cursor.line)
    var start = cursor.ch, end = cursor.ch
    while (start && /[\w@]/.test(line.charAt(start - 1))) --start
    while (end < line.length && /\w/.test(line.charAt(end))) ++end
    var word = line.slice(start, end).toLowerCase()
    if (word.startsWith('@')) { // check if term starts with @, those are articles.
        var term = word.slice(1,end-start);
        var result = [];
        //
        $.ajax({ // this is all async
            url: "https://www.worldanvil.com/api/article/bbcode.json",
            dataType: 'json',
            data: {q : term},
            success: function(data) {
                $.each(data, function(i,e) {
                    var row = {text: "@"+e['bbcode'], displayText: e['title']};
                    result.push(row);
                    //if (result.length < 5 ) { result.push(e['bbcode']);}
                });
                callback( {list: result,
                           from: CodeMirror.Pos(cursor.line, start),
                           to: CodeMirror.Pos(cursor.line, end)})
            }
        });
    } // until here
    else
    {
        for (var i = 0; i < comp.length; i++) if (comp[i].indexOf(word) != -1)
            callback({list: comp[i],
                      from: CodeMirror.Pos(cursor.line, start),
                      to: CodeMirror.Pos(cursor.line, end)})
    }
}
synonyms.async = true;



// Adding proper styling for the editor
GM_addStyle("/* BASICS */.CodeMirror {  /* Set height, width, borders, and global font properties here */  font-family: monospace;  height: 300px;  color: black;  direction: ltr;}/* PADDING */.CodeMirror-lines {  padding: 4px 0; /* Vertical padding around content */}.CodeMirror pre.CodeMirror-line,.CodeMirror pre.CodeMirror-line-like {  padding: 0 4px; /* Horizontal padding of content */}.CodeMirror-scrollbar-filler, .CodeMirror-gutter-filler {  background-color: white; /* The little square between H and V scrollbars */}/* GUTTER */.CodeMirror-gutters {  border-right: 1px solid #ddd;  background-color: #f7f7f7;  white-space: nowrap;}.CodeMirror-linenumbers {}.CodeMirror-linenumber {  padding: 0 3px 0 5px;  min-width: 20px;  text-align: right;  color: #999;  white-space: nowrap;}.CodeMirror-guttermarker { color: black; }.CodeMirror-guttermarker-subtle { color: #999; }/* CURSOR */.CodeMirror-cursor {  border-left: 1px solid black;  border-right: none;  width: 0;}/* Shown when moving in bi-directional text */.CodeMirror div.CodeMirror-secondarycursor {  border-left: 1px solid silver;}.cm-fat-cursor .CodeMirror-cursor {  width: auto;  border: 0 !important;  background: #7e7;}.cm-fat-cursor div.CodeMirror-cursors {  z-index: 1;}.cm-fat-cursor-mark {  background-color: rgba(20, 255, 20, 0.5);  -webkit-animation: blink 1.06s steps(1) infinite;  -moz-animation: blink 1.06s steps(1) infinite;  animation: blink 1.06s steps(1) infinite;}.cm-animate-fat-cursor {  width: auto;  border: 0;  -webkit-animation: blink 1.06s steps(1) infinite;  -moz-animation: blink 1.06s steps(1) infinite;  animation: blink 1.06s steps(1) infinite;  background-color: #7e7;}@-moz-keyframes blink {  0% {}  50% { background-color: transparent; }  100% {}}@-webkit-keyframes blink {  0% {}  50% { background-color: transparent; }  100% {}}@keyframes blink {  0% {}  50% { background-color: transparent; }  100% {}}/* Can style cursor different in overwrite (non-insert) mode */.CodeMirror-overwrite .CodeMirror-cursor {}.cm-tab { display: inline-block; text-decoration: inherit; }.CodeMirror-rulers {  position: absolute;  left: 0; right: 0; top: -50px; bottom: 0;  overflow: hidden;}.CodeMirror-ruler {  border-left: 1px solid #ccc;  top: 0; bottom: 0;  position: absolute;}/* DEFAULT THEME */.cm-s-default .cm-header {color: blue;}.cm-s-default .cm-quote {color: #090;}.cm-negative {color: #d44;}.cm-positive {color: #292;}.cm-header, .cm-strong {font-weight: bold;}.cm-em {font-style: italic;}.cm-link {text-decoration: underline;}.cm-strikethrough {text-decoration: line-through;}.cm-s-default .cm-keyword {color: #708;}.cm-s-default .cm-atom {color: #219;}.cm-s-default .cm-number {color: #164;}.cm-s-default .cm-def {color: #00f;}.cm-s-default .cm-variable,.cm-s-default .cm-punctuation,.cm-s-default .cm-property,.cm-s-default .cm-operator {}.cm-s-default .cm-variable-2 {color: #05a;}.cm-s-default .cm-variable-3, .cm-s-default .cm-type {color: #085;}.cm-s-default .cm-comment {color: #a50;}.cm-s-default .cm-string {color: #a11;}.cm-s-default .cm-string-2 {color: #f50;}.cm-s-default .cm-meta {color: #555;}.cm-s-default .cm-qualifier {color: #555;}.cm-s-default .cm-builtin {color: #30a;}.cm-s-default .cm-bracket {color: #997;}.cm-s-default .cm-tag {color: #170;}.cm-s-default .cm-attribute {color: #00c;}.cm-s-default .cm-hr {color: #999;}.cm-s-default .cm-link {color: #00c;}.cm-s-default .cm-error {color: #f00;}.cm-invalidchar {color: #f00;}.CodeMirror-composing { border-bottom: 2px solid; }/* Default styles for common addons */div.CodeMirror span.CodeMirror-matchingbracket {color: #0b0;}div.CodeMirror span.CodeMirror-nonmatchingbracket {color: #a22;}.CodeMirror-matchingtag { background: rgba(255, 150, 0, .3); }.CodeMirror-activeline-background {background: #e8f2ff;}/* STOP *//* The rest of this file contains styles related to the mechanics of   the editor. You probably shouldn't touch them. */.CodeMirror {  position: relative;  overflow: hidden;  background: white;}.CodeMirror-scroll {  overflow: scroll !important; /* Things will break if this is overridden */  /* 30px is the magic margin used to hide the element's real scrollbars */  /* See overflow: hidden in .CodeMirror */  margin-bottom: -30px; margin-right: -30px;  padding-bottom: 30px;  height: 100%;  outline: none; /* Prevent dragging from highlighting the element */  position: relative;}.CodeMirror-sizer {  position: relative;  border-right: 30px solid transparent;}/* The fake, visible scrollbars. Used to force redraw during scrolling   before actual scrolling happens, thus preventing shaking and   flickering artifacts. */.CodeMirror-vscrollbar, .CodeMirror-hscrollbar, .CodeMirror-scrollbar-filler, .CodeMirror-gutter-filler {  position: absolute;  z-index: 6;  display: none;}.CodeMirror-vscrollbar {  right: 0; top: 0;  overflow-x: hidden;  overflow-y: scroll;}.CodeMirror-hscrollbar {  bottom: 0; left: 0;  overflow-y: hidden;  overflow-x: scroll;}.CodeMirror-scrollbar-filler {  right: 0; bottom: 0;}.CodeMirror-gutter-filler {  left: 0; bottom: 0;}.CodeMirror-gutters {  position: absolute; left: 0; top: 0;  min-height: 100%;  z-index: 3;}.CodeMirror-gutter {  white-space: normal;  height: 100%;  display: inline-block;  vertical-align: top;  margin-bottom: -30px;}.CodeMirror-gutter-wrapper {  position: absolute;  z-index: 4;  background: none !important;  border: none !important;}.CodeMirror-gutter-background {  position: absolute;  top: 0; bottom: 0;  z-index: 4;}.CodeMirror-gutter-elt {  position: absolute;  cursor: default;  z-index: 4;}.CodeMirror-gutter-wrapper ::selection { background-color: transparent }.CodeMirror-gutter-wrapper ::-moz-selection { background-color: transparent }.CodeMirror-lines {  cursor: text;  min-height: 1px; /* prevents collapsing before first draw */}.CodeMirror pre.CodeMirror-line,.CodeMirror pre.CodeMirror-line-like {  /* Reset some styles that the rest of the page might have set */  -moz-border-radius: 0; -webkit-border-radius: 0; border-radius: 0;  border-width: 0;  background: transparent;  font-family: inherit;  font-size: inherit;  margin: 0;  white-space: pre;  word-wrap: normal;  line-height: inherit;  color: inherit;  z-index: 2;  position: relative;  overflow: visible;  -webkit-tap-highlight-color: transparent;  -webkit-font-variant-ligatures: contextual;  font-variant-ligatures: contextual;}.CodeMirror-wrap pre.CodeMirror-line,.CodeMirror-wrap pre.CodeMirror-line-like {  word-wrap: break-word;  white-space: pre-wrap;  word-break: normal;}.CodeMirror-linebackground {  position: absolute;  left: 0; right: 0; top: 0; bottom: 0;  z-index: 0;}.CodeMirror-linewidget {  position: relative;  z-index: 2;  padding: 0.1px; /* Force widget margins to stay inside of the container */}.CodeMirror-widget {}.CodeMirror-rtl pre { direction: rtl; }.CodeMirror-code {  outline: none;}/* Force content-box sizing for the elements where we expect it */.CodeMirror-scroll,.CodeMirror-sizer,.CodeMirror-gutter,.CodeMirror-gutters,.CodeMirror-linenumber {  -moz-box-sizing: content-box;  box-sizing: content-box;}.CodeMirror-measure {  position: absolute;  width: 100%;  height: 0;  overflow: hidden;  visibility: hidden;}.CodeMirror-cursor {  position: absolute;  pointer-events: none;}.CodeMirror-measure pre { position: static; }div.CodeMirror-cursors {  visibility: hidden;  position: relative;  z-index: 3;}div.CodeMirror-dragcursors {  visibility: visible;}.CodeMirror-focused div.CodeMirror-cursors {  visibility: visible;}.CodeMirror-selected { background: #d9d9d9; }.CodeMirror-focused .CodeMirror-selected { background: #d7d4f0; }.CodeMirror-crosshair { cursor: crosshair; }.CodeMirror-line::selection, .CodeMirror-line > span::selection, .CodeMirror-line > span > span::selection { background: #d7d4f0; }.CodeMirror-line::-moz-selection, .CodeMirror-line > span::-moz-selection, .CodeMirror-line > span > span::-moz-selection { background: #d7d4f0; }.cm-searching {  background-color: #ffa;  background-color: rgba(255, 255, 0, .4);}/* Used to force a border model for a node */.cm-force-border { padding-right: .1px; }@media print {  /* Hide the cursor when printing */  .CodeMirror div.CodeMirror-cursors {    visibility: hidden;  }}/* See issue #2901 */.cm-tab-wrap-hack:after { content: ''; }/* Help users use markselection to safely style text background */span.CodeMirror-selectedtext { background: none; }");
// Add darcula theme
GM_addStyle("/**    Name: IntelliJ IDEA darcula theme    From IntelliJ IDEA by JetBrains */.cm-s-darcula  { font-family: Consolas, Menlo, Monaco, 'Lucida Console', 'Liberation Mono', 'DejaVu Sans Mono', 'Bitstream Vera Sans Mono', 'Courier New', monospace, serif;}.cm-s-darcula.CodeMirror { background: #2B2B2B; color: #A9B7C6; }.cm-s-darcula span.cm-meta { color: #BBB529; }.cm-s-darcula span.cm-number { color: #6897BB; }.cm-s-darcula span.cm-keyword { color: #CC7832; line-height: 1em; font-weight: bold; }.cm-s-darcula span.cm-def { color: #A9B7C6; font-style: italic; }.cm-s-darcula span.cm-variable { color: #A9B7C6; }.cm-s-darcula span.cm-variable-2 { color: #A9B7C6; }.cm-s-darcula span.cm-variable-3 { color: #9876AA; }.cm-s-darcula span.cm-type { color: #AABBCC; font-weight: bold; }.cm-s-darcula span.cm-property { color: #FFC66D; }.cm-s-darcula span.cm-operator { color: #A9B7C6; }.cm-s-darcula span.cm-string { color: #6A8759; }.cm-s-darcula span.cm-string-2 { color: #6A8759; }.cm-s-darcula span.cm-comment { color: #61A151; font-style: italic; }.cm-s-darcula span.cm-link { color: #CC7832; }.cm-s-darcula span.cm-atom { color: #CC7832; }.cm-s-darcula span.cm-error { color: #BC3F3C; }.cm-s-darcula span.cm-tag { color: #629755; font-weight: bold; font-style: italic; text-decoration: underline; }.cm-s-darcula span.cm-attribute { color: #6897bb; }.cm-s-darcula span.cm-qualifier { color: #6A8759; }.cm-s-darcula span.cm-bracket { color: #A9B7C6; }.cm-s-darcula span.cm-builtin { color: #FF9E59; }.cm-s-darcula span.cm-special { color: #FF9E59; }.cm-s-darcula span.cm-matchhighlight { color: #FFFFFF; background-color: rgba(50, 89, 48, .7); font-weight: normal;}.cm-s-darcula span.cm-searching { color: #FFFFFF; background-color: rgba(61, 115, 59, .7); font-weight: normal;}.cm-s-darcula .CodeMirror-cursor { border-left: 1px solid #A9B7C6; }.cm-s-darcula .CodeMirror-activeline-background { background: #323232; }.cm-s-darcula .CodeMirror-gutters { background: #313335; border-right: 1px solid #313335; }.cm-s-darcula .CodeMirror-guttermarker { color: #FFEE80; }.cm-s-darcula .CodeMirror-guttermarker-subtle { color: #D0D0D0; }.cm-s-darcula .CodeMirrir-linenumber { color: #606366; }.cm-s-darcula .CodeMirror-matchingbracket { background-color: #3B514D; color: #FFEF28 !important; font-weight: bold; }.cm-s-darcula div.CodeMirror-selected { background: #214283; }.CodeMirror-hints.darcula {  font-family: Menlo, Monaco, Consolas, 'Courier New', monospace;  color: #9C9E9E;  background-color: #3B3E3F !important;}.CodeMirror-hints.darcula .CodeMirror-hint-active {  background-color: #494D4E !important;  color: #9C9E9E !important;}");
// Styling for the hint screen
GM_addStyle(".CodeMirror-hints {  position: absolute;  z-index: 10;  overflow: hidden;  list-style: none;  margin: 0;  padding: 2px;  -webkit-box-shadow: 2px 3px 5px rgba(0,0,0,.2);  -moz-box-shadow: 2px 3px 5px rgba(0,0,0,.2);  box-shadow: 2px 3px 5px rgba(0,0,0,.2);  border-radius: 3px;  border: 1px solid silver;  background: white;  font-size: 90%;  font-family: monospace;  max-height: 20em;  overflow-y: auto;}.CodeMirror-hint {  margin: 0;  padding: 0 4px;  border-radius: 2px;  white-space: pre;  color: black;  cursor: pointer;}li.CodeMirror-hint-active {  background: #08f;  color: white;}");

// Create editor
function createStuff() {
    codemirrorInstance = []; // create empty array for all instances.
    var foundtextareasarr = document.getElementsByClassName('mention'); // get all elements carrying the mention class, which should be all textareas
    for(var i = 0; foundtextareasarr[i]; ++i) {
        codemirrorInstance[i] = CodeMirror.fromTextArea(foundtextareasarr[i], {
            mode           : "bbcode",
            theme          : "darcula",
            tabSize        : 2,
            indentUnit     : 2,
            indentWithTabs : false,
            lineNumbers    : true,
            lineWrapping   : true,
            matchTags: {bothTags: true},
            extraKeys      : {
                "Ctrl-Space": "autocomplete",
            },
            hintOptions    : {
                hint: synonyms,
                async: true
            },
        }).on('keyup', function(i,e) {
            updateSaving();
            console.log('Saving status: '+unsaved);
        });
    }
}
(function() {
    'use strict';
    setTimeout(createStuff, 1000); // let page load first, then we do our stuff.
})();


$("<div class=\"ibox ibox-content\" id=\"sidebarstuff\">"+sidebarcontent+"</div>").insertBefore(".article-todo-wrapper");

function updateSaving() {
    if (!unsaved) {
        unsafeWindow.onbeforeunload = function(e) {
            return true;
        }
    }
    unsaved = true;
}
$("input.btn.btn-primary.btn-sm.btn-block.mb-2").first().click(function(e){
    unsafeWindow.onbeforeunload = undefined;
});
